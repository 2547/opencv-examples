import cv2
from helpers.loadImageHelper import *

# Cria duas mascaras e aplica na imagem abaixo.

frame = open_image('ZtgSK.jpg')

h, w = frame.shape[:2]
mask_color = (0, 50, 255) # isolar uma constante que repete

upper_mask = cv2.rectangle(frame, (0, 0), (w, int(0.5 * h)), mask_color, -1)
lower_mask = cv2.rectangle(frame, (0, h), (w, int(0.7 * h)), mask_color, -1)

cv2.imshow('output', frame)

cv2.waitKey(0)
cv2.destroyAllWindows()
