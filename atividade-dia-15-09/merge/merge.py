import cv2
from helpers.loadImageHelper import *

img = open_image('lenna.jpeg')

b, g, r = cv2.split(img)

cv2.imshow('image', b)
cv2.waitKey(0)
cv2.imshow('image1', g)
cv2.waitKey(0)
cv2.imshow('image2', r)
cv2.waitKey(0)


image = cv2.merge((b, g, r))
cv2.imshow('image', image)
cv2.waitKey(0)
cv2.destroyWindows()
