import cv2

# Select a ROI and then press SPACE or ENTER button!
# Cancel the selection process by pressing c button!

# Read image
im = cv2.imread("lenna.jpeg")

# Select ROI
r = cv2.selectROI(im)

# Crop image
imCrop = im[int(r[1]):int(r[1]+r[3]), int(r[0]):int(r[0]+r[2])]

# Display cropped image
cv2.imshow("Image", imCrop)
cv2.waitKey(0)
