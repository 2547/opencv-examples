import cv2 as cv
import numpy as np

global X, Y


# mouse function here
def mouse(event, x, y, flags, param):
    global X, Y
    if event == cv.EVENT_LBUTTONDBLCLK:
        cv.circle(img, (x, y), 100, (255, 0, 0), -1)
    if event == cv.EVENT_LBUTTONDOWN:
        X = x
        Y = y
    # if event == cv.EVENT_MOUSEMOVE:
    # X=x
    # Y=y
    if event == cv.EVENT_LBUTTONUP:
        cv.rectangle(img, (X, Y), (x, y), (255, 0, 0), -1)


img = np.zeros((512, 512, 3), np.uint8)
font = cv.FONT_HERSHEY_SIMPLEX
cor = (255, 0, 0)
img = cv.putText(img, 'Aula', (50, 50), font, 1, cor, 2, cv.LINE_AA)
cv.namedWindow("Original")
cv.setMouseCallback("Original", mouse)
while 1:
    cv.imshow('Original', img)
    if cv.waitKey(20) & 0xFF == 27:
        break
cv.destroyAllWindows()
