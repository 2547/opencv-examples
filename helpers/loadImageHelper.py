import cv2 as cv


def open_image(path):
    src = cv.imread(path, cv.IMREAD_COLOR)

    if src is None:
        print('Could not open or find the image: ', path)
        exit(0)

    return src
