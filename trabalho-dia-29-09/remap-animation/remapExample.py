from __future__ import print_function
import argparse
import cv2 as cv
import numpy as np

# Criação de movimento a partir de imagens estáticas.

def update_map(ind, map_x, map_y):
    if ind == 0:
        for i in range(map_x.shape[0]):
            for j in range(map_x.shape[1]):
                if map_x.shape[1] * 0.25 < j < map_x.shape[1] * 0.75 and map_x.shape[0] * 0.25 < i < map_x.shape[0] * 0.75:
                    map_x[i, j] = 2 * (j - map_x.shape[1] * 0.25) + 0.5
                    map_y[i, j] = 2 * (i - map_y.shape[0] * 0.25) + 0.5
                else:
                    map_x[i, j] = 0
                    map_y[i, j] = 0
    elif ind == 1:
        for i in range(map_x.shape[0]):
            map_x[i, :] = [x for x in range(map_x.shape[1])]
        for j in range(map_y.shape[1]):
            map_y[:, j] = [map_y.shape[0] - y for y in range(map_y.shape[0])]
    elif ind == 2:
        for i in range(map_x.shape[0]):
            map_x[i, :] = [map_x.shape[1] - x for x in range(map_x.shape[1])]
        for j in range(map_y.shape[1]):
            map_y[:, j] = [y for y in range(map_y.shape[0])]
    elif ind == 3:
        for i in range(map_x.shape[0]):
            map_x[i, :] = [map_x.shape[1] - x for x in range(map_x.shape[1])]
        for j in range(map_y.shape[1]):
            map_y[:, j] = [map_y.shape[0] - y for y in range(map_y.shape[0])]


parser = argparse.ArgumentParser(description='Code for Remapping tutorial.')
parser.add_argument('--input', help='Path to input image.', default='chicky_512.png')
args = parser.parse_args()

imagePath = "images/dart-logo.png"

src = cv.imread(imagePath, cv.IMREAD_COLOR)
if src is None:
    print('Could not open or find the image: ', imagePath)
    exit(0)

#Cria a imagem de destino e as duas matrizes de mapemamento
map_x = np.zeros((src.shape[0], src.shape[1]), dtype=np.float32)
map_y = np.zeros((src.shape[0], src.shape[1]), dtype=np.float32)

#Cria uma janela para mostrar os resultados
window_name = 'Remap demo'
cv.namedWindow(window_name)

#Estabelce um loop. A cada 1000 ms, atualizamos nossas matrizes de mapas (mat_x and mat_y) e aplicamos elas para nossa imagem inicialmente carregada.
ind = 0
while True:
    update_map(ind, map_x, map_y)
    ind = (ind + 1) % 4
    dst = cv.remap(src, map_x, map_y, cv.INTER_LINEAR)
    cv.imshow(window_name, dst)
    c = cv.waitKey(1000)
    if c == 27:
        break

#Referência do codigo utilizado
#https://docs.opencv.org/3.4/d1/da0/tutorial_remap.html
